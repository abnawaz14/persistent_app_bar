import 'package:custom_navigator/custom_navigator.dart';
import 'package:floating_bottom_navigation_bar/floating_bottom_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_nav_bar/screens/downloads_page.dart';
import 'package:flutter_nav_bar/screens/event_page.dart';
import 'package:flutter_nav_bar/screens/home_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _currentIndex = 0;

  final navigatorKeys = [
    GlobalKey<NavigatorState>(),
    GlobalKey<NavigatorState>(),
    GlobalKey<NavigatorState>(),
  ];

  List<FloatingNavbarItem> _items = [
    FloatingNavbarItem(icon: Icons.home, title: 'home'),
    FloatingNavbarItem(icon: Icons.event, title: 'events'),
    FloatingNavbarItem(icon: Icons.save_alt, title: 'downloads'),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: FloatingNavbar(
          onTap: (int index) {
            setState(() {
              _currentIndex = index;
            });
          },
          currentIndex: _currentIndex,
          items: _items,
        ),
        // bottomNavigationBar: BottomNavigationBar(
        //   items: _items,
        //   onTap: (index) {
        //     setState(() {
        //       _currentIndex = index;
        //     });
        //   },
        //   currentIndex: _currentIndex,
        // ),
        body: IndexedStack(
          index: _currentIndex,
          children: <Widget>[
            _buildPage(navigatorKeys[0], 0),
            _buildPage(navigatorKeys[1], 1),
            _buildPage(navigatorKeys[2], 2),
          ],
        ));
  }

  Widget _buildPage(GlobalKey<NavigatorState> key, int index) {
    return CustomNavigator(
      navigatorKey: key,
      home: showPage(index),
      pageRoute: PageRoutes.materialPageRoute,
    );
  }

  Widget showPage(int index) {
    switch (index) {
      case 0:
        return HomePage();
        break;

      case 1:
        return EventPage();
        break;

      case 2:
        return DownloadsPage();
        break;

      default:
        return HomePage();
        break;
    }
  }
}
