import 'package:flutter/material.dart';
import 'package:flutter_nav_bar/screens/event_Details.dart';

class EventPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Center(
          child: GestureDetector(
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => EventDetails()));
            },
            child: Text(
              "Event",
              textScaleFactor: 1.5,
            ),
          ),
        ),
      ),
    );
  }
}
