import 'package:flutter/material.dart';
import 'package:flutter_nav_bar/screens/download_details.dart';

class DownloadsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Center(
          child: GestureDetector(
            onTap: () {
              Navigator.of(context, rootNavigator: true).push(
                  MaterialPageRoute(builder: (context) => DownloadDetails()));
            },
            child: Text(
              "Downloads",
              textScaleFactor: 1.5,
            ),
          ),
        ),
      ),
    );
  }
}
